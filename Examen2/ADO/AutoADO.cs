﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Examen2.Models;

namespace Examen2.ADO
{
	public class AutoADO
	{
		SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["CNX"].ConnectionString);

		public int Insertar(Auto a)
		{
			int resultado = -1;
			SqlCommand cmd = new SqlCommand("SP_AUTO_INSERTAR", cn);
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.Parameters.AddWithValue("@placa", a.Placa);
			cmd.Parameters.AddWithValue("@anio", a.Año);
			cmd.Parameters.AddWithValue("@modelo", a.Modelo);
			cmd.Parameters.AddWithValue("@idmarca", a.idMarca);

			cn.Open();
			resultado = cmd.ExecuteNonQuery();
			cn.Close();
			return resultado;
		}

		public List<Auto> Listar()
		{
			List<Auto> lista = new List<Auto>();
			SqlCommand cmd = new SqlCommand("SP_AUTO_LISTAR", cn);
			cmd.CommandType = CommandType.StoredProcedure;
			cn.Open();
			SqlDataReader dr = cmd.ExecuteReader();
			while (dr.Read())
			{
				lista.Add(new Auto()
				{
					idAuto = dr.GetInt32(0),
					NombreMarca = dr.GetString(1),
					Modelo = dr.GetString(2),
					Placa = dr.GetString(3),
					Año = dr.GetInt32(4),
					idMarca = dr.GetInt32(5)

				});
			}
			dr.Close();
			cn.Close();
			return lista;
		}

		public int Eliminar(int id)
		{
			int resultado = -1;
			SqlCommand cmd = new SqlCommand("SP_AUTO_ELIMINAR", cn);
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.Parameters.AddWithValue("@idauto", id);

			cn.Open();
			resultado = cmd.ExecuteNonQuery();
			cn.Close();
			return resultado;
		}

		public Auto Obtener(int id)
		{
			Auto obj = new Auto();
			SqlCommand cmd = new SqlCommand("SP_AUTO_OBTENER", cn);
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.Parameters.AddWithValue("@idauto", id);
			cn.Open();
			SqlDataReader dr = cmd.ExecuteReader();
			while (dr.Read())
			{
				obj.idAuto = dr.GetInt32(0);
				obj.NombreMarca = dr.GetString(1);
				obj.Modelo = dr.GetString(2);
				obj.Placa = dr.GetString(3);
				obj.Año = dr.GetInt32(4);
			}
			dr.Close();
			cn.Close();
			return obj;
		}

		public int Actualizar(Auto a)
		{
			int resultado = -1;
			SqlCommand cmd = new SqlCommand("SP_AUTO_ACTUALIZAR", cn);
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.Parameters.AddWithValue("@placa", a.Placa);
			cmd.Parameters.AddWithValue("@anio", a.Año);
			cmd.Parameters.AddWithValue("@modelo", a.Modelo);
			cmd.Parameters.AddWithValue("@idmarca", a.idMarca);
			cmd.Parameters.AddWithValue("@idauto", a.idAuto);

			cn.Open();
			resultado = cmd.ExecuteNonQuery();
			cn.Close();
			return resultado;

		}
	}
}