﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Examen2.Models;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Examen2.Controllers
{
	public class ViajesController : Controller
	{

		SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["CNX"].ConnectionString);
		List<Viaje> BuscarViajesXAnio(string anio)
		{
			List<Viaje> resultado = new List<Viaje>();
			SqlCommand cmd = new SqlCommand("SP_VIAJES_X_ANIO", cn);
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.Parameters.AddWithValue("@anio", anio);
			cn.Open();
			SqlDataReader dr = cmd.ExecuteReader();
			while (dr.Read())
			{
				Viaje v = new Viaje();
				v.ViajeId = dr.GetInt32(0);
				v.Origen = dr.GetString(1);
				v.Destino = dr.GetString(2);
				v.FechaPartida = dr.GetDateTime(3);
				v.FechaLLegada = dr.GetDateTime(4);
				v.NombreConductor = dr.GetString(5);
				v.ModeloAuto = dr.GetString(6);
				resultado.Add(v);

			}
			dr.Close();
			cn.Close();
			return resultado;
		}

		List<Viaje> BuscarViajesXConductorYAnio(string conductor, string anio)
		{
			List<Viaje> resultado = new List<Viaje>();
			SqlCommand cmd = new SqlCommand("SP_VIAJES_X_CONDUCTOR_Y_ANIO_PARTIDA", cn);
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.Parameters.AddWithValue("@conductor", conductor);
			cmd.Parameters.AddWithValue("@anio", anio);
			cn.Open();
			SqlDataReader dr = cmd.ExecuteReader();
			while (dr.Read())
			{
				Viaje v = new Viaje();
				v.ViajeId = dr.GetInt32(0);
				v.Origen = dr.GetString(1);
				v.Destino = dr.GetString(2);
				v.FechaPartida = dr.GetDateTime(3);
				v.FechaLLegada = dr.GetDateTime(4);
				v.NombreConductor = dr.GetString(5);
				v.ModeloAuto = dr.GetString(6);
				resultado.Add(v);

			}
			dr.Close();
			cn.Close();
			return resultado;
		}

		List<Viaje> BuscarViajesXAutoYAnio(string auto, string anio)
		{
			List<Viaje> resultado = new List<Viaje>();
			SqlCommand cmd = new SqlCommand("SP_VIAJES_X_AUTO_Y_ANIO_PARTIDA", cn);
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.Parameters.AddWithValue("@auto", auto);
			cmd.Parameters.AddWithValue("@anio", anio);
			cn.Open();
			SqlDataReader dr = cmd.ExecuteReader();
			while (dr.Read())
			{
				Viaje v = new Viaje();
				v.ViajeId = dr.GetInt32(0);
				v.Origen = dr.GetString(1);
				v.Destino = dr.GetString(2);
				v.FechaPartida = dr.GetDateTime(3);
				v.FechaLLegada = dr.GetDateTime(4);
				v.NombreConductor = dr.GetString(5);
				v.ModeloAuto = dr.GetString(6);
				resultado.Add(v);

			}
			dr.Close();
			cn.Close();
			return resultado;
		}

		public ActionResult Index()
		{
			return View();
		}

		public ActionResult BuscarViajesPorAnio(string anio = null)
		{
			List<Viaje> lista = new List<Viaje>();
			if(!string.IsNullOrEmpty(anio))
				lista = BuscarViajesXAnio(anio);
			return View(lista);
		}

		public ActionResult BuscarViajesPorConductorYAnio(string conductor = null, string anio = null)
		{
			List<Viaje> lista = new List<Viaje>();
			if (!string.IsNullOrEmpty(conductor) && !string.IsNullOrEmpty(anio))
				lista = BuscarViajesXConductorYAnio(conductor, anio);
			return View(lista);
		}

		public ActionResult BuscarViajesPorAutoYAnio(string auto = null, string anio = null)
		{
			List<Viaje> lista = new List<Viaje>();
			if (!string.IsNullOrEmpty(auto) && !string.IsNullOrEmpty(anio))
				lista = BuscarViajesXAutoYAnio(auto, anio);
			return View(lista);
		}

	}
}