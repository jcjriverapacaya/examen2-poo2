﻿using Examen2.ADO;
using Examen2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Examen2.Controllers
{
    public class VehiculoController : Controller
    {
        // GET: Vehiculo
        public ActionResult Index()
        {
            return View();
        }

        // Declarar los ADOS
        AutoADO adoAuto = new AutoADO();
        MarcaADO adoMarca = new MarcaADO();
        public ActionResult ListarAutos()
        {
            List<Auto> lista = adoAuto.Listar();
            return View(lista);
        }

        public ActionResult RegistrarAuto()
        {
            ViewBag.marcas = adoMarca.Listar();
            return View(new Auto());
        }

        [HttpPost]
        public ActionResult RegistrarAuto(Auto a)
        {
            int resultado = adoAuto.Insertar(a);
            return RedirectToAction("ListarAutos");
        }

        public ActionResult EliminarAuto(int id)
        {
            int resultado = adoAuto.Eliminar(id);
            return RedirectToAction("ListarAutos");
        }

        public ActionResult VerAuto(int id)
        {
            Auto a = adoAuto.Obtener(id);
            return View(a);
        }
        public ActionResult EditarAuto(int id)
        {
            Auto a = adoAuto.Obtener(id);
            ViewBag.marcas = adoMarca.Listar();

            return View(a);
        }
        [HttpPost]
        public ActionResult EditarAuto(Auto a)
        {
            int resultado = adoAuto.Actualizar(a);
            return RedirectToAction("ListarAutos");
        }
    }
}