﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Examen2.Models
{
	public class Viaje
	{
		public int ViajeId { get; set; }
		public string Origen { get; set; }
		public string Destino { get; set; }

		public DateTime FechaPartida { get; set; }
		public DateTime FechaLLegada { get; set; }
		public string NombreConductor { get; set; }
		public string ModeloAuto { get; set; }
	}
}