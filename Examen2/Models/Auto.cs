﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Examen2.Models
{
	public partial class Auto
	{
		[Key]
		[Display(Name ="Id")]
		public int idAuto { get; set; }
		public string Modelo { get; set; }
		public string Placa { get; set; }
		public int Año { get; set; }

		[Display(Name ="Marca")]
		public int idMarca { get; set; }
	}
}